const fs = require('fs')
const path = require('path')
const emoticons = require('./src/emoticons.json')

writeEmoticons('index.html', 'dist', generateEmoticonList(emoticons))

/**
 * Returns a single emoticon item
 * @param  {object} emoticon Single emoticon object
 * @return {String}
 */
function generateEmoticonItem (emoticon) {
  const style = `style="color: ${emoticon.color || 'currentColor'}"`

  return `<li ${style}>
    <h2>${emoticon.figure}</h2>
    <div>
      <span>${emoticon.text.ENG}</span>
      <button>Copy</button>
    </div>
  </li>`
}

/**
 * Returns the template for a list of emoticons
 * @param  {array}  list Array of emoticons
 * @return {String}
 */
function generateEmoticonList (list) {
  let listTemplate = ''
  list.forEach(item => listTemplate += generateEmoticonItem(item))

  return `<ul>${listTemplate}</ul>`
}

/**
 * Write a emoticon list to disk
 * @param  {String} sourceFile Source file
 * @param  {String} targetPath Path to target
 * @param  {String} template   Template to be written
 */
function writeEmoticons (sourceFile, targetPath, template) {
  fs.readFile(path.resolve(__dirname, sourceFile), {encoding: 'utf8'}, (err, data) => {
    const emoticonList = data.replace('{{ emoticonList }}', template)
    const dist = path.resolve(__dirname, targetPath, sourceFile)

    fs.writeFile(dist, emoticonList, {encoding: 'utf8'})
  })
}
