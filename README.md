# Arabic Script Emoticons

A series of Emoticons using only Arabic Script.

Hosted on [arabic-emoticons.worksonmymachine.sh](arabic-emoticons.worksonmymachine.sh).

## Why

Finding myself stuck in Tehrans traffic I couldn't help but notice that some of the number plates had little faces and other symbold on them. So here we are. Yay. Know others, add them!

## Develop

```bash
# Develop
yarn start

# Deploy
yarn deploy

# Manually build
yarn build
```
